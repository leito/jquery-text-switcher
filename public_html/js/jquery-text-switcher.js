/*  This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
(function ($) {

    /**
     * Creates a text than changes values on click, using a predefined set of
     * possible values.
     * The possible values and its corresponding CSS class is defined using
     * data-option-values and data-option-classes (a comma-separated list each).
     * Settigns:
     *  defaultOptions: an array of Options used when no data-option-values is present.
     *  onChange: a function to invoke when a value is changed.
     *  onChangeDelay: the ammount of inactivity (in milliseconds) before calling onChange.
     */
    $.fn.textSwitcher = function (customSettings) {
        var settings = $.extend({
            defaultOptions: [
                {value: "PENDING", class: "label-default"},
                {value: "APPROVED", class: "label-success"},
                {value: "REJECTED", class: "label-danger"}
            ],
            onChangeDelay: 1000
        }, customSettings);


        this.each(function () {
            var $container = $(this);
            var options = findOptionsForElement($container, settings);
            var option = findOption(options, $container.html());
            if (!option) {
                option = options[0];
            }
            $container.html(option.value);
            $container.addClass(option.class);

            var optionChanged = throttle(function () {
                if (settings.onChange) {
                    settings.onChange($container);
                }
            }, settings.onChangeDelay);

            $container.click(function () {
                var currentValue = $container.html();
                var nextOption = findNextOption(options, currentValue);
                removeAllClass($container, options);
                $container.addClass(nextOption.class);
                $container.html(nextOption.value);

                optionChanged();
            });
        });

        return this;
    };

    /** Finds the options for the current $container. If no options is defined
     * using data-options-values and data-option-classes, the default options
     * is returned.
     */
    function findOptionsForElement($container, settings) {
        var optionValues = $container.data("option-values");
        var optionClasses;
        var values, classes, options, i;
        if (optionValues) {
            values = optionValues.split(",");
            optionClasses = $container.data("option-classes");
            classes = optionClasses.split(",");
            options = [];
            for (i = 0; i < values.length; i++) {
                options.push({
                    value: values[i],
                    class: classes[i]
                });
            }
        }
        else {
            options = settings.defaultOptions;
        }
        return options;
    }

    function findNextOption(options, currentValue) {
        var i;
        var next;
        for (i = 0; i < options.length; i++) {
            if (options[i].value === currentValue) {
                break;
            }
        }
        next = i + 1;
        if (next >= options.length) {
            next = 0;
        }
        return options[next];
    }

    function findOption(options, value) {
        var i;
        for (i = 0; i < options.length; i++) {
            if (options[i].value === value) {
                return options[i];
            }
        }
        return null;
    }

    function removeAllClass($container, values) {
        var i;
        for (i = 0; i < values.length; i++) {
            $container.removeClass(values[i].class);
        }
    }


    /**
     * Demora la ejeuccion de una funcion una cierta cantidad de tiempo (en
     * milisegundos). Si se realiza otra invocacion antes de este tiempo,
     * se cancela la primer invocación, se resetea el timer y se pone la nueva
     * invocacion en espera.
     * Por ejemplo, es útil para "ejecutar una búsqueda 2 segundos después del
     * ultimo keypress".
     * Leer mas: http://stackoverflow.com/questions/4364729/jquery-run-code-2-seconds-after-last-keypress
     *
     * @param f la funcion a invocar.
     * @param delay la demora en milisegundos. Default: 500.
     */
    function throttle(f, delay) {
        var timer = null;
        return function () {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = window.setTimeout(function () {
                f.apply(context, args);
            },
                    delay || 500);
        };
    }

}(jQuery));
